const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const TourSchema = new Schema(
	{
		name: { type: String, required: true },
		lodging_ids: [{ type: String }],
		availability: { type: Boolean, required: true },
		validity: [{ type: Date, required: true }],
		details: { type: String },
		inclusions: [{ type: String }],
		itinerary: [{ type: String }],
		// optional_tour: [{ type: String }],
		sub_category_id: { type: String, required: true },
		lodging_ids: [{ type: String }],
		tour_length: { type: String, required: true },
		price: { type: Number, required: true },
		image: { type: String }
	},
	{
		timestamps: true
	}
);

module.exports = mongoose.model("Tour", TourSchema);
