const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const SubCategorySchema = new Schema(
	{
		name: { type: String, required: true },
		category_id: { type: String, required: true }
	},
	{
		timestamps: true
	}
);

module.exports = mongoose.model("SubCategory", SubCategorySchema);
