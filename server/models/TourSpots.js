const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const TourSpotsSchema = new Schema({
	name : {type:String, required:true},
	location : {type:String, required:true},
	details : {type:String, required : true}
	},

	{
		timestamps: true
	});

module.exports = mongoose.model("TourSpots", TourSpotsSchema);