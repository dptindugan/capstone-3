const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const PartnerSchema = new Schema({
	name : {type:String, required:true},
	location : {type:String},
	business_type : {type:String, required : true},
	website : {type:String, required:true},
	details : {type:Date, required:true}
	},
	{
		timestamps: true
	});

module.exports = mongoose.model("Partner", PartnerSchema);