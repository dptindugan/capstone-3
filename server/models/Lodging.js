const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const LodgingSchema = new Schema(
	{
		name: { type: String, required: true },
		location_id: { type: String, required: true },
		city: { type: String, required: true }
	},
	{
		timestamps: true
	}
);

module.exports = mongoose.model("Lodging", LodgingSchema);
