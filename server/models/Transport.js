const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const TransportSchema = new Schema({
	transportation_code : {type:String, required:true},
	transport_type : {type:String, required:true},
	availability: {type:Boolean, required:true}
	},
	{
		timestamps: true
	});

module.exports = mongoose.model("Transport", TransportSchema);