const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const OptionalTourSchema = new Schema(
	{
		name: { type: String, required: true },
		details: { type: String, required: true },
		price: { type: Number, required: true },
		length_in_hour: { type: String },
		tour_id: { type: String }
	},
	{
		timestamps: true
	}
);

module.exports = mongoose.model("OptionalTour", OptionalTourSchema);
