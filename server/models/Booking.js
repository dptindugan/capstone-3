// require dependencies

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const BookingSchema = new Schema(
	{
		schedule: { type: Date },
		user_id: { type: String },
		service_id: { type: String },
		tour_id: { type: String }
	},
	{
		timestamps: true
	}
);

module.exports = mongoose.model("Booking", BookingSchema);
