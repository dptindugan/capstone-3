const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const UserSchema = new Schema(
	{
		firstName: { type: String, required: true },
		lastName: { type: String, required: true },
		username: { type: String, unique: true },
		email: { type: String, required: true },
		password: { type: String },
		contact_no: { type: String },
		role: { type: String }
	},
	{
		timestamps: true
	}
);

module.exports = mongoose.model("User", UserSchema);
