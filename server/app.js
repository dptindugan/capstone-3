const express = require("express");
const app = express();

const mongoose = require("mongoose");
const port = process.env.PORT || 7000;

let databaseURL =
	process.env.DATABASE_URL ||
	"mongodb+srv://seierherre:qgWujmRGrCfbPnTG@cluster0-zxhmp.mongodb.net/travelAndTours?retryWrites=true&w=majority";

mongoose.connect(databaseURL, {
	useCreateIndex: true,
	useNewUrlParser: true
});

const bodyParser = require("body-parser");

mongoose.connection.once("open", () => {
	console.log("Now connected to the online Mongodb server");
});

const cors = require("cors");

const server = require("./queries/queries.js");

app.use(bodyParser.json({ limit: "15mb" }));
app.use("/images", express.static("images"));

server.applyMiddleware({
	app,
	path: "/capstone3"
});

app.use(cors());

app.listen(port, () => {
	console.log(
		`🚀 Server ready at http://localhost:${port + server.graphqlPath}`
	);
});
