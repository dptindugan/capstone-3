const { ApolloServer, gql } = require("apollo-server-express");
const { GraphQLDateTime } = require("graphql-iso-date");
const bcrypt = require("bcrypt");

const Booking = require("../models/Booking");
const User = require("../models/User");
const Service = require("../models/Service");
const Category = require("../models/Category");
const SubCategory = require("../models/SubCategory");
const Tour = require("../models/Tour");
const Lodging = require("../models/Lodging");
const OptionalTour = require("../models/OptionalTour");

const uuid = require("uuid/v1");
const fs = require("fs");

const customScalarResolver = {
	Date: GraphQLDateTime
};

const typeDefs = gql`
	#define schemas

	scalar Date

	type SubCategoryType {
		id: ID!
		name: String!
		category_id: String!
		category: CategoryType
	}

	type OptionalTourType {
		id: ID!
		name: String!
		details: String!
		price: Float!
		length_in_hour: String
		tour_id: String!
	}

	type BookingType {
		id: ID
		service_id: String
		tour_id: String
		user_id: String
		user: UserType
		updatedAt: Date
		createdAt: Date
		schedule: Date
		service: ServiceType
		tour: TourType
	}

	type UserType {
		id: ID
		firstName: String!
		lastName: String!
		username: String
		email: String!
		password: String
		contact_no: String
		role: String
		createdAt: Date
		updatedAt: Date
		bookings: [BookingType]
	}

	type ServiceType {
		id: ID
		name: String!
		availability: Boolean!
		details: String!
		bookings: [BookingType]
		createdAt: Date
		updatedAt: Date
	}

	type TourType {
		id: ID!
		name: String!
		lodging_ids: [String]
		availability: Boolean!
		#validity: [Date]
		details: String
		inclusions: [String]
		optional_tour: [String]
		price: Float
		image: String
		itinerary: [String]
		sub_category_id: String!
		sub_category: SubCategoryType
		lodgings: [LodgingType]
		tour_length: String!
		bookings: [BookingType]
		createdAt: Date
		updatedAt: Date
	}

	type LodgingType {
		id: ID!
		name: String!
		location_id: String!
		city: String!
		location: LocationType
		tour: TourType
		createdAt: Date
		updatedAt: Date
	}

	type LocationType {
		id: ID!
		name: String!
		category_id: String!
		category: CategoryType
		createdAt: Date
		updatedAt: Date
	}

	type CategoryType {
		id: ID!
		name: String!
		createdAt: Date
		updatedAt: Date
	}

	type Mutation {
		#OptionalTour CUD
		createOptionalTour(
			name: String!
			details: String!
			price: Float!
			length_in_hour: String
			tour_id: String!
		): OptionalTourType

		updateOptionalTour(
			id: ID!
			name: String!
			details: String!
			price: Float!
			length_in_hour: String
			tour_id: String!
		): OptionalTourType

		#end optionalTour of CUD

		#tour CUD
		createTour(
			name: String!
			lodging_ids: [String]
			availability: Boolean!
			#validity: [Date]
			details: String
			inclusions: [String]
			itinerary: [String]
			sub_category_id: String!
			optional_tour: [String]
			tour_length: String!
			price: Float
			image: String
		): TourType

		updateTour(
			id: ID!
			name: String!
			lodging_ids: [String]
			availability: Boolean!
			#validity: [Date]
			details: String
			inclusions: [String]
			itinerary: [String]
			optional_tour: [String]
			sub_category_id: String!
			tour_length: String!
			price: Float
			image: String
		): TourType

		deleteTour(id: ID!): Boolean

		#end of tour CUD

		#Lodging CUD
		createLodging(
			name: String!
			location_id: String
			city: String!
		): LodgingType

		updateLodging(
			id: ID!
			name: String!
			location_id: String
			city: String!
		): LodgingType

		deleteLodging(id: ID!): Boolean

		#end of lodging CUD

		#category CUD
		createCategory(name: String!): CategoryType

		updateCategory(id: ID!, name: String!): CategoryType

		deleteCategory(id: ID!): Boolean
		#end of category CUD

		#subcategory CUD
		createSubCategory(name: String!, category_id: String!): SubCategoryType

		updateSubCategory(
			id: String!
			name: String!
			category_id: String
		): SubCategoryType

		deleteSubCategory(id: ID!): Boolean

		#end of SubCategory CUD

		# booking CUD
		createBooking(
			user_id: String
			service_id: String
			tour_id: String
			schedule: Date
		): BookingType

		updateBooking(
			id: ID!
			contact_no: String
			service_id: String
			tour_id: String
			schedule: Date
		): BookingType

		deleteBooking(id: ID!): Boolean
		# end of booking CUD

		# User CUD
		createUser(
			firstName: String!
			lastName: String!
			username: String
			email: String!
			password: String
			contact_no: String
			role: String
		): UserType

		updateUser(
			id: ID!
			firstName: String!
			lastName: String!
			username: String
			email: String!
			password: String
			contact_no: String
			role: String
		): UserType

		deleteUser(id: ID!): Boolean
		# end of booking CUD

		# service CUD
		createService(
			name: String!
			availability: Boolean
			details: String!
		): ServiceType

		updateService(
			id: ID!
			name: String!
			availability: Boolean!
			details: String!
		): ServiceType

		deleteService(id: ID!): Boolean
		#end of service CUD

		logInMember(username: String!, password: String!): UserType
	}

	type Query {
		getLodgings: [LodgingType]
		getLodging(id: ID!): LodgingType

		getBookings: [BookingType]
		getBooking(id: ID!): BookingType

		getTours: [TourType]
		getTour(id: ID!): TourType

		getUsers: [UserType]
		getUser(
			id: ID!
			username: String
			firstName: String
			lastName: String
			email: String
		): UserType

		getServices: [ServiceType]
		getService(id: ID!): ServiceType

		getCategories: [CategoryType]
		getCategory(id: ID!): CategoryType

		getSubCategories: [SubCategoryType]
		getSubCategory: SubCategoryType

		getOptionalTours: [OptionalTourType]
		getOptionalTour: OptionalTourType
	}
`;

const resolvers = {
	Query: {
		getSubCategories: () => {
			return SubCategory.find({});
		},

		getLodgings: () => {
			return Lodging.find({});
		},

		getLodging: (_, args) => {
			return Lodging.findById(args.id);
		},

		getCategories: () => {
			return Category.find({});
		},

		getCategory: (_, args) => {
			return Category.findById(args.id);
		},

		getBookings: () => {
			return Booking.find({});
		},

		getBooking: (_, args) => {
			return User.findById(args.id);
		},

		getUsers: () => {
			return User.find({});
		},

		getUser: (_, args) => {
			return User.findById(args.id);
		},

		getServices: () => {
			return Service.find({});
		},

		getService: (_, args) => {
			return Service.findById(args.id);
		},

		getTours: (_, args) => {
			return Tour.find({});
		},
		getTour: (_, args) => {
			return Tour.findById(args.id);
		},
		getOptionalTours: (_, args) => {
			return OptionalTour.find({});
		}
	},

	Mutation: {
		logInMember: (_, args) => {
			return User.findOne({ username: args.username }).then(user => {
				if (user === null) {
					return null;
				}

				if (bcrypt.compareSync(args.password, user.password)) {
					// console.log("login successful")
					return user;
				} else {
					// console.log("wrong password")
					return null;
				}
			});
		},

		createOptionalTour: (_, args) => {
			let newOptionalTour = new OptionalTour({
				name: args.name,
				details: args.details,
				price: args.price,
				length_in_hour: args.length_in_hour,
				tour_id: args.tour_id
			});
			return newOptionalTour.save();
		},

		updateOptionalTour: (_, args) => {
			return OptionalTour.findByIdAndUpdate(
				args.id,
				{
					$set: {
						name: args.name,
						details: args.details,
						price: args.price,
						length_in_hour: args.length_in_hour,
						tour_id: args.tour_id
					}
				},
				err => {
					if (err) {
						// console.log(err);
					}
					// console.log("update success");
				}
			);
		},

		createTour: (_, args) => {
			// console.log("creating tour");
			// console.log(args);
			let imageString = args.image;
			let imageBase = imageString.split(";base64,").pop();

			// console.log(imageString);

			let imageLocation = "images/" + uuid() + ".png";

			fs.writeFile(
				imageLocation,
				imageBase,
				{ encoding: "base64" },
				err => {}
			);

			let newTour = new Tour({
				name: args.name,
				lodging_ids: args.lodging_ids,
				availability: args.availability,
				// validity: args.validity,

				details: args.details,
				inclusions: args.inclusions,
				itinerary: args.itinerary,
				sub_category_id: args.sub_category_id,
				optional_tour: args.optional_tour,
				tour_length: args.tour_length,
				price: args.price,
				image: imageLocation
			});
			return newTour.save();
		},

		updateTour: (_, args) => {
			let imageString = args.image;
			let imageBase = imageString.split(";base64,").pop();

			// console.log(imageString);

			let imageLocation = "images/" + uuid() + ".png";

			fs.writeFile(
				imageLocation,
				imageBase,
				{ encoding: "base64" },
				err => {}
			);
			return Tour.findByIdAndUpdate(
				args.id,
				{
					$set: {
						name: args.name,
						lodging_ids: args.lodging_ids,
						availability: args.availability,
						// validity: args.validity,
						details: args.details,
						inclusions: args.inclusions,
						itinerary: args.itinerary,
						optional_tour: args.optional_tour,
						sub_category_id: args.sub_category_id,
						tour_length: args.tour_length,
						price: args.price,
						image: imageLocation
					}
				},
				err => {
					if (err) {
						// console.log(err);
					}
					// console.log("update success");
				}
			);
		},

		deleteTour: (_, args) => {
			return Tour.findByIdAndDelete(args.id).then((lodging, err) => {
				if (err || !lodging) {
					// console.log("delete fail")
					return false;
				} else {
					// console.log("delete success")
					return true;
				}
			});
		},

		createLodging: (_, args) => {
			let newLodging = new Lodging({
				name: args.name,
				location_id: args.location_id,
				city: args.city
			});
			return newLodging.save();
		},

		updateLodging: (_, args) => {
			return Lodging.findByIdAndUpdate(
				args.id,
				{
					$set: {
						name: args.name,
						location_id: args.location_id,
						city: args.city
					}
				},
				err => {
					if (err) {
						// console.log(err);
					}
					// console.log("update success");
				}
			);
		},

		deleteLodging: (_, args) => {
			return Lodging.findByIdAndDelete(args.id).then((lodging, err) => {
				if (err || !lodging) {
					// console.log("delete fail")
					return false;
				} else {
					// console.log("delete success")
					return true;
				}
			});
		},

		createCategory: (_, args) => {
			let newCategory = new Category({
				name: args.name
			});
			return newCategory.save();
		},

		createSubCategory: (_, args) => {
			let newSubCategory = new SubCategory({
				name: args.name,
				category_id: args.name
			});
			return newSubCategory.save();
		},

		updateCategory: (_, args) => {
			return Category.findByIdAndUpdate(
				args.id,
				{
					$set: {
						name: args.name
					}
				},
				err => {
					if (err) {
						// console.log(err);
					}
					// console.log("update success");
				}
			);
		},

		createBooking: (_, args) => {
			let newBooking = new Booking({
				service_id: args.service_id,
				tour_id: args.tour_id,
				user_id: args.user_id,
				schedule: args.schedule
			});
			return newBooking.save();
		},

		updateBooking: (_, args) => {
			return Booking.findByIdAndUpdate(
				args.id,
				{
					$set: {
						service_id: args.service_id,
						tour_id: args.tour_id,
						user_id: args.user_id,
						schedule: args.schedule
					}
				},
				err => {
					if (err) {
						// console.log(err);
					}
					// console.log("update success");
				}
			);
		},

		createUser: (_, args) => {
			let newUser = new User({
				firstName: args.firstName,
				lastName: args.lastName,
				username: args.username,
				email: args.email,
				contact_no: args.contact_no,
				password: bcrypt.hashSync(args.password, 10),
				role: args.role
			});
			return newUser.save();
		},

		updateUser: (_, args) => {
			return User.findByIdAndUpdate(args.id, {
				$set: {
					firstName: args.firstName,
					lastName: args.lastName,
					username: args.username,
					email: args.email,
					contact_no: args.contact,
					password: bcrypt.hashSync(args.password, 10),
					role: args.role
				}
			});
		},

		createService: (_, args) => {
			let newService = new Service({
				name: args.name,
				image: args.image,
				service_type: args.service_type,
				availability: true,
				details: args.details
			});
			return newService.save();
		},

		updateService: (_, args) => {
			return Service.findByIdAndUpdate(args.id, {
				$set: {
					name: args.name,
					image: args.image,
					service_type: args.service_type,
					availability: args.availability,
					details: args.details
				}
			});
		}
	},

	BookingType: {
		service: (parent, args) => {
			return Service.findById(parent.service_id);
		},
		user: (parent, args) => {
			return User.findById(parent.user_id);
		},
		tour: (parent, args) => {
			return Tour.findById(parent.tour_id);
		}
	},

	ServiceType: {
		bookings: (parent, args) => {
			return Booking.find({ service_id: parent.id });
		}
	},

	TourType: {
		bookings: (parent, args) => {
			return Booking.find({ id: parent.id });
		},
		lodgings: (parent, args) => {
			// console.log(parent.lodging_ids);
			// console.log(args);
			// lodging associated to the tour
			// lodging._id tourtype[lodging_ids]
			// console.log(Lodging.find({ id: { _in: [parent.lodging_ids] } }));
			// return Lodging.find({ _id: parent.lodging_ids });
			// return Tour.find({ lodging_ids : args.id);
		}
	},

	UserType: {
		bookings: (parent, args) => {
			return Booking.find({ user_id: parent.id });
		}
	}
};

// create an instance of apollo server
const server = new ApolloServer({
	typeDefs,
	resolvers
});

// create a custom type resolver to get data of a defType from another

// export module
module.exports = server;
