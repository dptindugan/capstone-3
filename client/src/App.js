import React, { useState } from "react";
import "react-bulma-components/dist/react-bulma-components.min.css";
import PageContent from "./components/user/Index";
import Dashboard from "./components/admin/Dashboard";
import "bootstrap/dist/css/bootstrap.min.css";
import "react-datepicker/dist/react-datepicker.css";
import "./App.css";
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "react-apollo";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import CreateLodging from "./components/admin/CreateLodging";
import CreateTour from "./components/admin/CreateTour";
import UpdateTour from "./components/admin/UpdateTour";
import UpdateLodging from "./components/admin/UpdateLodging";
import Tour from "./components/user/Tour";
// import NavBar from "./components/NavBar";
import Login from "./components/Login";

const client = new ApolloClient({
	uri: "https://pacific-refuge-63569.herokuapp.com/capstone3"
});

function App() {
	// console.log(username);
	const [username, setUsername] = useState("");

	if (username === "") {
		if (localStorage.getItem("role")) {
			setUsername(localStorage.getItem("role"));
		}
	}

	let content = username === "admin" ? Dashboard : PageContent;

	return (
		<ApolloProvider client={client}>
			<BrowserRouter>
				<Switch>
					<Route exact path="/" component={content} />
					<Route exact path="/tours/create" component={CreateTour} />
					<Route
						exact
						path="/lodgings/create"
						component={CreateLodging}
					/>
					<Route exact path="/login" component={Login} />
					<Route
						exact
						path="/lodging/update/:id"
						component={UpdateLodging}
					/>
					<Route exact path="/tour/show/:name/:id" component={Tour} />
					<Route
						exact
						path="/tour/update/:id"
						component={UpdateTour}
					/>
				</Switch>
			</BrowserRouter>
		</ApolloProvider>
	);
}

export default App;
