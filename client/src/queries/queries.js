import { gql } from "apollo-boost";

const getToursQuery = gql`
    {
        getTours {
            id
            name
            lodging_ids
            availability
            details
            image
            inclusions
            optional_tour
            itinerary
            sub_category_id
            tour_length
            createdAt
            updatedAt
        }
    }
`;

const getLodgingsQuery = gql`
    {
        getLodgings {
            id
            name
            location_id
            city
            location {
                name
            }
            tour {
                name
            }
            createdAt
            updatedAt
        }
    }
`;

const getTourQuery = gql`
    query($id: ID!) {
        getTour(id: $id) {
            id
            name
            lodging_ids
            availability
            details
            inclusions
            optional_tour
            itinerary
            sub_category_id
            tour_length
            price
            createdAt
            updatedAt
            image
        }
    }
`;

const getLodgingQuery = gql`
    query($id: ID!) {
        getLodging(id: $id) {
            id
            city
            name
            location_id
        }
    }
`;

const getBookingsQuery = gql`
    {
        getBookings {
            id
            service_id
            tour_id
            user_id
            schedule
            tour {
                id
                name
                lodgings {
                    name
                }
                itinerary
                inclusions
                price
                optional_tour
            }
            user {
                id
                lastName
                firstName
                username
                email
                contact_no
                role
            }
        }
    }
`;

export {
    getToursQuery,
    getLodgingsQuery,
    getTourQuery,
    getLodgingQuery,
    getBookingsQuery
};
