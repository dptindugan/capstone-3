import { gql } from "apollo-boost";

const deleteLodgingMutation = gql`
	mutation($id: ID!) {
		deleteLodging(id: $id)
	}
`;

const deleteTourMutation = gql`
	mutation($id: ID!) {
		deleteTour(id: $id)
	}
`;

const createLodgingMutation = gql`
	mutation($name: String!, $city: String!, $location_id: String!) {
		createLodging(name: $name, city: $city, location_id: $location_id) {
			id
			name
			city
			location_id
		}
	}
`;

const createTourMutation = gql`
	mutation(
		$details: String!
		$name: String!
		$inclusions: [String]
		$itinerary: [String]
		$lodging_ids: [String]
		$availability: Boolean!
		$sub_category_id: String!
		$tour_length: String!
		$price: Float
		$image: String
	) {
		createTour(
			details: $details
			name: $name
			inclusions: $inclusions
			itinerary: $itinerary
			lodging_ids: $lodging_ids
			availability: $availability
			sub_category_id: $sub_category_id
			tour_length: $tour_length
			price: $price
			image: $image
		) {
			id
			details
			name
			inclusions
			itinerary
			lodging_ids
			availability
			sub_category_id
			tour_length
			price
			image
		}
	}
`;

const updateTourMutation = gql`
	mutation(
		$id: ID!
		$details: String!
		$name: String!
		$inclusions: [String]
		$itinerary: [String]
		$lodging_ids: [String]
		$availability: Boolean!
		$sub_category_id: String!
		$tour_length: String!
		$price: Float
		$image: String
	) {
		updateTour(
			id: $id
			details: $details
			name: $name
			inclusions: $inclusions
			itinerary: $itinerary
			lodging_ids: $lodging_ids
			availability: $availability
			sub_category_id: $sub_category_id
			tour_length: $tour_length
			price: $price
			image: $image
		) {
			id
			details
			name
			inclusions
			itinerary
			lodging_ids
			availability
			sub_category_id
			tour_length
			price
			image
		}
	}
`;

const updateLodgingMutation = gql`
	mutation($id: ID!, $name: String!, $city: String!, $location_id: String!) {
		updateLodging(
			id: $id
			name: $name
			city: $city
			location_id: $location_id
		) {
			id
			name
			city
			location_id
		}
	}
`;

const createBookingMutation = gql`
	mutation(
		$service_id: String
		$tour_id: String
		$user_id: String
		$schedule: Date
	) {
		createBooking(
			service_id: $service_id
			tour_id: $tour_id
			user_id: $user_id
			schedule: $schedule
		) {
			id
			service_id
			tour_id
			user_id
			schedule
		}
	}
`;

const createUserMutation = gql`
	mutation(
		$firstName: String!
		$lastName: String!
		$username: String
		$email: String!
		$password: String
		$contact_no: String
		$role: String
	) {
		createUser(
			firstName: $firstName
			lastName: $lastName
			username: $username
			email: $email
			password: $password
			contact_no: $contact_no
			role: $role
		) {
			id
			firstName
			lastName
			username
			email
			password
			contact_no
			role
		}
	}
`;

const logInMutation = gql`
	mutation($username: String!, $password: String!) {
		logInMember(username: $username, password: $password) {
			id
			firstName
			lastName
			password
			username
			email
			contact_no
			role
		}
	}
`;

export {
	deleteLodgingMutation,
	deleteTourMutation,
	createLodgingMutation,
	createTourMutation,
	updateTourMutation,
	updateLodgingMutation,
	createBookingMutation,
	createUserMutation,
	logInMutation
};
