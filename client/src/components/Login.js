import React, { useState } from "react";
import { Card } from "react-bulma-components";
import { graphql } from "react-apollo";
// import { Redirect } from "react-router-dom";
import { logInMutation } from "../queries/mutation";
import Swal from "sweetalert2";
import NavBar from "./user/NavBar";

const Login = props => {
	// console.log(props);

	const [state, setState] = useState({
		username: "",
		password: "",
		logInSuccess: false
	});

	const inputHandler = e => {
		const value = e.target.value;
		setState({
			...state,
			[e.target.name]: value
		});
	};

	const submitFormHandler = e => {
		e.preventDefault();
		let credentials = {
			username: state.username,
			password: state.password
		};

		props
			.logInMutation({
				variables: credentials
			})
			.then(res => {
				let data = res.data.logInMember;
				// console.log(data);
				if (data === null) {
					Swal.fire({
						title: "Login Failed",
						text: "wrong credentials",
						icon: "error"
					});
				} else {
					alert("success");
					// setItem("key", "value")
					localStorage.setItem("username", data.username);
					localStorage.setItem("firstName", data.firstName);
					localStorage.setItem("lastName", data.lastName);
					localStorage.setItem("role", data.role);

					setState({
						...state,
						logInSuccess: true
					});
					window.location.href = "/";
				}
			});
	};

	// if (state.logInSuccess) {
	// 	return <Redirect to="/" />;
	// }

	return (
		<div>
			<NavBar username="" />
			<div className="inputFormLogin">
				<Card>
					{/*<div className="imgContainer">
									<i class="fas fa-user-tie fa-10x" />
								</div>*/}
					<Card.Header>
						<Card.Header.Title>Login</Card.Header.Title>
					</Card.Header>
					<Card.Content>
						<form onSubmit={submitFormHandler}>
							<div className="field loginInput">
								<input
									className="input"
									type="text"
									id="username"
									name="username"
									onChange={inputHandler}
									required
								/>
								<label
									htmlFor="username"
									className="loginLabel"
								>
									Username
								</label>
							</div>
							<div className="loginInput">
								<input
									type="password"
									id="password"
									name="password"
									className="input"
									onChange={inputHandler}
									required
								/>
								<label
									htmlFor="password"
									className="loginLabel"
								>
									Password
								</label>
							</div>
							<button className="button is-primary is-outlined is-fullwidth">
								Login
							</button>
						</form>
					</Card.Content>
				</Card>
			</div>
		</div>
	);
};

export default graphql(logInMutation, { name: "logInMutation" })(Login);
