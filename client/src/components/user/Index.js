import React from "react";
import ImageTransitions from "./ImageTransitions";
import Tours from "./Tours";
import Services from "./Services";
import About from "./About";
import NavBar from "../NavBar";

const PageContent = e => {
	return (
		<div>
			<NavBar username={localStorage.getItem("firstName")} />
			<ImageTransitions />
			<Tours />
			<About />
			<Services />
		</div>
	);
};

export default PageContent;
