import React from "react";
import { Section, Container, Heading } from "react-bulma-components";

const Services = props => {
	return (
		<Section>
			<Container>
				<div>
					<Heading className="has-text-centered">Services</Heading>
					<Heading
						subtitle
						className="has-text-centered has-text-grey-light"
					>
						We offer the Following services
					</Heading>
				</div>
			</Container>
		</Section>
	);
};

export default Services;
