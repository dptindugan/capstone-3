import React, { useState, useEffect } from "react";
import { useTransition, animated, config } from "react-spring";
import { Container, Heading, Columns } from "react-bulma-components";
import { Link } from "react-router-dom";

const ImageTransitions = () => {
	const slides = [
		{
			id: 0,
			url: "/images/baguio.jpg",
			title: "Baguio Tour",
			message: "Chill, Relax and whatever in Baguio",
			link: "/tour/show/Baguio%20Tour%20Package/5def52c0e177194965f291c6"
		},
		{
			id: 1,
			url: "/images/Cagsawaruins.jpg",
			title: "Bicol Tour",
			message: "Go and see the beauty of bicol province",
			link: "/tour/show/Bicol%20Tour%20Package/5de873e9bdb4d3714325fa6e"
		},
		{
			id: 2,
			url: "/images/davao-bamboo.jpg",
			title: "Davao Tour",
			message: "Life is Here",
			link: "/tour/show/Davao%20Tour%20Package/5def5345e177194965f291c7"
		}
	];

	const [index, set] = useState(0);
	const transitions = useTransition(slides[index], item => item.id, {
		from: { opacity: 0 },
		enter: { opacity: 1 },
		leave: { opacity: 0 },
		config: config.molasses
	});
	useEffect(
		() => void setInterval(() => set(state => (state + 1) % 3), 4000),
		[]
	);
	return transitions.map(({ item, props, key }) => (
		<animated.div
			key={key}
			className="bg"
			style={{
				...props,
				backgroundImage: `linear-gradient(rgba(127,255,212, 0.35), rgba(255,99,71,0.25)), url(${item.url})`,
				backgroundPosition: "center",
				backgroundRepeat: "no-repeat",
				backgroundSize: "cover"
			}}
		>
			<Container className="transitionsContainer">
				<Columns>
					<Columns.Column>
						<Heading subtitle size={3} className="transitionHeader">
							{item.title}
						</Heading>
						<Heading
							color="white"
							size={2}
							className="transitionsContent"
						>
							{item.message}
						</Heading>
						<Link
							to={item.link}
							className="button is-light is-large is-outlined"
						>
							Read More
						</Link>
					</Columns.Column>
				</Columns>
			</Container>
		</animated.div>
	));
};

export default ImageTransitions;
