import React from "react";
import { Section, Container, Heading } from "react-bulma-components";

const About = props => {
	return (
		<Section className="has-background-primary">
			<Container>
				<Heading>About</Heading>
				<p>
					S T&T is a booking website app for your "travel" and "tour"
					needs, but note that this app is still the demo phase
				</p>
				<p>
					<strong>Disclaimer:</strong>No Copyright Intended, I do not
					own any of the images nor profit from them
				</p>
			</Container>
		</Section>
	);
};

export default About;
