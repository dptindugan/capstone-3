import React, { useState } from "react";
import { Navbar } from "react-bulma-components";
import { getToursQuery } from "../../queries/queries";
import { graphql } from "react-apollo";
import { Link } from "react-router-dom";

const NavBar = props => {
	// console.log(window.pageYOffset);
	const [navPos, setNavPos] = useState("navws");

	window.onscroll = function() {
		if (window.pageYOffset === 0) {
			setNavPos("navws");
			return true;
		} else {
			setNavPos("");
		}
	};

	const logoutHandler = () => {
		localStorage.clear();
		window.location.href = "/login";
	};

	let customLink = "";

	if (props.username) {
		customLink = (
			<Navbar.Item dropdown hoverable href="#">
				<Navbar.Link
					arrowless={false}
				>{`Hi ${props.username}!`}</Navbar.Link>
				<Navbar.Dropdown>
					<Navbar.Item onClick={logoutHandler}>Logout</Navbar.Item>
				</Navbar.Dropdown>
			</Navbar.Item>
		);
	} else {
		customLink = (
			<Navbar.Container position="end">
				<Link to="/Login" className="navbar-item">
					Log In
				</Link>
				<Link to="/" className="navbar-item">
					Sign Up
				</Link>
			</Navbar.Container>
		);
	}

	let tours = props.getToursQuery.getTours
		? props.getToursQuery.getTours
		: [];

	// console.log(tours);
	return (
		<Navbar id="nav" fixed="top" className={navPos}>
			<Navbar.Brand className="navBrand">
				<Link className="navbar-item" to="/">
					<i>
						T<span className="fas fa-compass" />
					</i>
				</Link>
				<Navbar.Burger />
			</Navbar.Brand>
			<Navbar.Menu>
				<Navbar.Container position="end">
					<Navbar.Item dropdown hoverable href="#">
						<Navbar.Link arrowless={false}>Tours</Navbar.Link>
						<Navbar.Dropdown>
							{tours.map(tour => {
								return (
									<Link
										to={`/tour/show/${tour.name}/${tour.id}`}
										key={tour.id}
										className="navbar-item"
									>
										{tour.name}
									</Link>
								);
							})}
						</Navbar.Dropdown>
					</Navbar.Item>
					{customLink}
				</Navbar.Container>
			</Navbar.Menu>
		</Navbar>
	);
};

export default graphql(getToursQuery, { name: "getToursQuery" })(NavBar);
