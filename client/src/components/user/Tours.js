import React from "react";
import {
	Section,
	Heading,
	Image,
	Columns,
	Container
} from "react-bulma-components";
import { getToursQuery } from "../../queries/queries";
import { graphql } from "react-apollo";
import "react-bulma-components/dist/react-bulma-components.min.css";
import { Link } from "react-router-dom";
import { nodeServer } from "../../function.js";

const Tours = props => {
	// console.log(props);

	let toursData = !props.getToursQuery.loading
		? props.getToursQuery.getTours
		: [];
	return (
		<Section className="tours">
			<Container>
				<div>
					<Heading className="has-text-centered">Our Tours</Heading>
					<Heading
						subtitle
						className="has-text-centered has-text-grey-light"
					>
						Check Out Our Tour Packages
					</Heading>
				</div>
				<Columns>
					{toursData.map((tour, key) => {
						return (
							<Columns.Column key={key} size={3}>
								<Link to={`/tour/show/${tour.name}/${tour.id}`}>
									<Image
										size="4by3"
										src={nodeServer() + tour.image}
										style={{ objectFit: "cover" }}
									/>
								</Link>
							</Columns.Column>
						);
					})}
				</Columns>
			</Container>
		</Section>
	);
};

export default graphql(getToursQuery, { name: "getToursQuery" })(Tours);
