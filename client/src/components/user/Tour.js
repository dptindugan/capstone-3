import React, { useState } from "react";
import { getTourQuery, getToursQuery } from "../../queries/queries";
import { flowRight } from "lodash";
import {
	Container,
	Hero,
	Heading,
	Columns,
	Box,
	Section,
	Card,
	Button
} from "react-bulma-components";
import { graphql } from "react-apollo";
import {
	createBookingMutation,
	createUserMutation
} from "../../queries/mutation";
import NavBar from "./NavBar";
import { Carousel } from "react-bootstrap";
import DatePicker from "react-datepicker";
import { format } from "date-fns";
import { Link } from "react-router-dom";
import { nodeServer } from "../../function.js";
import Swal from "sweetalert2";

const Tour = props => {
	// console.log(props.getTourQuery.getTour);
	// console.log(tourData);

	let toursData = !props.getToursQuery.loading
		? props.getToursQuery.getTours
		: [];
	let tourData = props.getTourQuery.getTour ? props.getTourQuery.getTour : {};
	let inclusions = tourData.inclusions ? tourData.inclusions : [];

	// console.log(props.getTourQuery.loading);

	let [state, setState] = useState({
		tourId: "",
		firstName: "",
		lastName: "",
		email: "",
		contact_no: "",
		startDate: new Date(),
		role: "guest"
	});

	if (!props.getTourQuery.loading) {
		if (state.tourId === "") {
			setState({ ...state, tourId: tourData.id });
		}
	}

	// console.log(state.tourId);

	let onChangeHandler = e => {
		let value = e.target.value;
		setState({
			...state,
			[e.target.id]: value
		});
	};

	// console.log(props.createBookingMutation);
	// console.log(props.createUserMutation);

	// console.log(format(parseISO(state.startDate, "MMMM d, yyyy")));

	// console.log(state.startDate);

	let onChangeDateHandler = date => {
		setState({
			...state,
			startDate: date
		});
	};

	let onSubmitHandler = e => {
		e.preventDefault();

		let userInfo = {
			firstName: state.firstName,
			lastName: state.lastName,
			email: state.email,
			contact_no: state.contact_no,
			role: state.role,
			password: "guest"
		};

		props
			.createUserMutation({
				variables: userInfo
			})
			.then((res, err) => {
				let bookingInfo = {
					tour_id: state.tourId,
					user_id: res.data.createUser.id,
					schedule: format(state.startDate, "MMMM d, yyyy")
				};
				// console.log(res.data.createUser.id);
				// console.log(bookingInfo);
				if (err) {
					Swal.fire({
						icon: "error",
						title: "Oops...",
						text: "Something went wrong!"
					});
				}

				props
					.createBookingMutation({
						variables: bookingInfo
					})
					.then((res, err) => {
						if (err) {
							Swal.fire({
								icon: "error",
								title: "Oops...",
								text: "Something went wrong!"
							});
						} else {
							// Swal.fire({
							// 	title: "Successfully Booked!",
							// 	text: "Please Check Your Email For Details",
							// 	imageUrl: res.data.createBooking.image,
							// 	imageWidth: 400,
							// 	imageHeight: 200,
							// 	imageAlt: "Custom image"
							// });
							Swal.fire({
								title: "Tour Successfully Booked",
								text: "Please check your email for details",
								icon: "success",

								// first approach
								html:
									'<a href="/" class="button is-success">Confirm</a>',
								showCancelButton: false,
								showConfirmButton: false

								// // or
								// confirmButtonText:
								// 	'<a href="/" class="has-text-white">Go back to members </a>'
							});
						}
					});
			});
	};
	// console.log(tourData);

	return (
		<section>
			<NavBar />
			<Hero
				style={{
					backgroundImage: `url(${nodeServer() + tourData.image})`,
					backgroundSize: "cover",
					backgroundPosition: "center",
					backgroundAttachment: "fixed"
				}}
			>
				<Hero.Body>
					<Container className="has-text-centered">
						<Heading>{tourData.name}</Heading>
						<Heading subtitle size={3}>
							{tourData.tour_length}
						</Heading>
					</Container>
				</Hero.Body>
			</Hero>
			<Container className="my-5">
				<Columns>
					<Columns.Column className="is-two-thirds">
						<Heading>{tourData.name}</Heading>
						<Heading subtitle size={5}>
							{tourData.tour_length}
						</Heading>
						<Carousel>
							<Carousel.Item>
								<img
									style={{
										maxHeight: "360px",
										objectFit: "cover"
									}}
									className="d-block w-100"
									src="/images/baguio.jpg"
									alt="First slide"
								/>
							</Carousel.Item>
							<Carousel.Item>
								<img
									style={{
										maxHeight: "360px",
										objectFit: "cover"
									}}
									className="d-block w-100"
									src="/images/Cagsawaruins.jpg"
									alt="Third slide"
								/>
							</Carousel.Item>
							<Carousel.Item>
								<img
									style={{
										maxHeight: "360px",
										objectFit: "cover"
									}}
									className="d-block w-100"
									src="/images/davao-bamboo.jpg"
									alt="Third slide"
								/>
							</Carousel.Item>
						</Carousel>
						<Box>
							<Heading>Inclusions</Heading>
							<ul>
								{inclusions.map((inclusion, key) => {
									return <li key={key}>{inclusion}</li>;
								})}
							</ul>
						</Box>
					</Columns.Column>
					<Columns.Column>
						<Box>
							<Heading size={5}>Price</Heading>
							<span>
								<strong>
									&#x20b1;{" "}
									{tourData.price
										? tourData.price.toFixed(2)
										: ""}
								</strong>{" "}
								Per Person
							</span>
						</Box>

						<Box>
							<Heading size={4}>Book This Tour</Heading>
							<form
								className="inputForm"
								onSubmit={onSubmitHandler}
							>
								<div className="field userInfo">
									<input
										type="text"
										id="firstName"
										className="input"
										onChange={onChangeHandler}
										value={state.firstName}
										required
									/>
									<label
										htmlFor="firstName"
										className="userInfoLabel"
									>
										First Name
									</label>
								</div>
								<div className="field userInfo">
									<input
										type="text"
										id="lastName"
										className="input"
										onChange={onChangeHandler}
										value={state.lastName}
										required
									/>
									<label
										htmlFor="lastName"
										className="userInfoLabel"
									>
										Last Name
									</label>
								</div>
								<div className="field userInfo">
									<input
										type="email"
										id="email"
										className="input"
										onChange={onChangeHandler}
										value={state.email}
										required
									/>
									<label
										htmlFor="email"
										className="userInfoLabel"
										required
									>
										Email
									</label>
								</div>
								<div className="field userInfo">
									<input
										type="text"
										id="contact_no"
										className="input"
										onChange={onChangeHandler}
										value={state.contact_no}
										required
									/>
									<label
										htmlFor="contact_no"
										className="userInfoLabel"
									>
										Contact No.
									</label>
								</div>
								<div className="field">
									<label htmlFor="schedule" className="label">
										Tour Date
									</label>
									<DatePicker
										id="schedule"
										className="input"
										selected={state.startDate}
										onChange={onChangeDateHandler}
									/>
								</div>
								<Columns>
									<Columns.Column>
										<Button fullwidth color="info">
											Book Now
										</Button>
									</Columns.Column>
									<Columns.Column>
										<Button
											fullwidth
											type="button"
											color="primary"
										>
											Inquire
										</Button>
									</Columns.Column>
								</Columns>
							</form>
						</Box>
					</Columns.Column>
				</Columns>
				<Section>
					<Heading size={4}>You May Also Like</Heading>
					<Columns>
						{toursData.map((tour, key) => {
							return (
								<Columns.Column key={key} size={3}>
									<Link
										to={`/tour/show/${tour.name}/${tour.id}`}
									>
										<Card>
											<Card.Image
												size="4by3"
												style={{ objectFit: "cover" }}
												src={nodeServer() + tour.image}
											/>
											<Card.Footer>
												{tour.name}
											</Card.Footer>
										</Card>
									</Link>
								</Columns.Column>
							);
						})}
					</Columns>
				</Section>
			</Container>
		</section>
	);
};

export default flowRight(
	graphql(getTourQuery, {
		options: props => {
			// retrieve the wildcard id param
			let id = props.match.params.id;
			return {
				variables: { id: id }
			};
		},
		name: "getTourQuery"
	}),
	graphql(createBookingMutation, { name: "createBookingMutation" }),
	graphql(createUserMutation, { name: "createUserMutation" }),
	graphql(getToursQuery, { name: "getToursQuery" })
)(Tour);
