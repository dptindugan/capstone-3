import React from "react"
import { Container, Heading, Box, Tabs, Table, Button } from "react-bulma-components";


const Settings = ({actionBtn}, props) =>{
	// console.log(props)
	// console.log(actionBtn)

	return (
		<Container>
			<Box>
				<Heading size={4} className="has-text-grey-light">Settings</Heading>
				<Tabs type="toggle">
					<Tabs.Tab>Item1</Tabs.Tab>

				</Tabs>
				<Table>
					<thead>
						<tr>
							<td>Name</td>
							<td>Name</td>
							<td>Name</td>
							<td>Action</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Settings 1</td>
							<td>asldfjlsdkfjlk</td>
							<td>asldfjlsdkfjlk</td>
							<td>			
								<Button
									id="updateBtn" 
									color="success"
									className="actionButton update-btn"
									onClick={actionBtn}
								>
									<i className="far fa-edit fa-lg updateIconBtn"></i>
									<span className="tooltip" id="update-tip">update</span>
								</Button>
							
								<Button
									id="deleteBtn"
									color="danger"
									className="actionButton delete-btn"
									onClick={actionBtn}>
									<i className="fas fa-trash-alt fa-lg deleteIconBtn"></i>
									<span className="tooltip" id="delete-tip">delete</span>
								</Button>
							</td>
						</tr>
					</tbody>
				</Table>
			</Box>
		</Container>
	)
}

export default Settings;