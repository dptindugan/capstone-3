import React, { useState } from "react";
import { updateLodgingMutation } from "../../queries/mutation";
import { getLodgingQuery } from "../../queries/queries";
import { Container, Columns, Button, Heading } from "react-bulma-components";
import { flowRight } from "lodash";
import { graphql } from "react-apollo";
import Swal from "sweetalert2";

const UpdateLodging = props => {
	const [state, setState] = useState({
		id: "",
		name: "",
		city: "",
		location_id: ""
	});

	let defaultDatas = {};
	if (!props.getLodgingQuery.loading) {
		let lodgingData = props.getLodgingQuery.getLodging;
		// console.log(lodgingData);
		defaultDatas = {
			id: lodgingData.id,
			name: lodgingData.name,
			city: lodgingData.city,
			location_id: lodgingData.location_id
		};

		const setDefaultData = () => {
			setState(defaultDatas);
		};

		if (state.id === "") {
			setDefaultData();
		}
	}

	const changeHandler = e => {
		let value = e.target.value;
		setState({
			...state,
			[e.target.name]: value
		});
	};

	const submitFormHandler = e => {
		e.preventDefault();

		if (
			defaultDatas.name === state.name &&
			defaultDatas.city === state.city &&
			defaultDatas.location_id === state.location_id
		) {
			Swal.fire({
				title: "Update Failed",
				text: "No changes made",
				type: "warning",
				icon: "warning",

				// first approach
				// html:
				// 	'<a href="/" class="button is-success">Go back to members </a>',
				showCancelButton: false,
				showConfirmButton: true

				// or
				// confirmButtonText:
				// 	'<a href="/" class="has-text-white">Go back to members </a>'
			});
		} else {
			let updateLodging = {
				id: state.id,
				name: state.name,
				city: state.city,
				location_id: state.location_id
			};
			if (
				!updateLodging.name ||
				!updateLodging.city ||
				!updateLodging.location_id
			) {
				Swal.fire({
					icon: "error",
					title: "Oops...",
					text: "Something went wrong!"
				});
			} else {
				props.updateLodgingMutation({
					variables: updateLodging
				});

				Swal.fire({
					title: "Tour Updated",
					text: "Tour has been updated",
					icon: "success",

					// first approach
					html:
						'<a href="/" class="button is-success">Go back Dashboard</a>',
					showCancelButton: false,
					showConfirmButton: false

					// // or
					// confirmButtonText:
					// 	'<a href="/" class="has-text-white">Go back to members </a>'
				});
			}
		}
	};

	return (
		<Container>
			<Columns>
				<Columns.Column>
					<Heading>Update Lodging</Heading>
					<form onSubmit={submitFormHandler}>
						<fieldset className="field">
							<label className="label" htmlFor="name">
								Name
							</label>
							<input
								id="name"
								className="input"
								name="name"
								onChange={changeHandler}
								value={state.name}
							/>
						</fieldset>
						<fieldset className="field">
							<label className="label" htmlFor="city">
								City
							</label>
							<input
								id="city"
								className="input"
								name="city"
								onChange={changeHandler}
								value={state.city}
							/>
						</fieldset>
						<fieldset className="field">
							<label className="label" htmlFor="location_id">
								Location
							</label>
							<input
								id="location_id"
								className="input"
								name="location_id"
								onChange={changeHandler}
								value={state.location_id}
							/>
						</fieldset>
						<Button color="info">Update Lodging</Button>
					</form>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default flowRight(
	graphql(updateLodgingMutation, {
		name: "updateLodgingMutation"
	}),
	graphql(getLodgingQuery, {
		options: props => {
			// retrieve the wildcard id param
			let id = props.match.params.id;
			return {
				variables: { id: id }
			};
		},
		name: "getLodgingQuery"
	})
)(UpdateLodging);
