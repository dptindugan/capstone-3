import React from "react";
import { Container, Heading, Box, Table, Button } from "react-bulma-components";
import { getBookingsQuery } from "../../queries/queries";
import { graphql } from "react-apollo";
import { format, parseISO } from "date-fns";

const Bookings = props => {
	// console.log(props.getBookingsQuery);

	let bookingData = !props.getBookingsQuery.loading
		? props.getBookingsQuery.getBookings
		: [];
	// console.log(bookingData);
	return (
		<Container>
			<Box>
				<Heading size={4} className="has-text-grey-light">
					Bookings
				</Heading>

				<Table>
					<thead>
						<tr>
							<td>Tour</td>
							<td>Tour Price</td>
							<td>Customer Name</td>
							<td>Email</td>
							<td>Contact No</td>
							<td>Schedule</td>
							<td>Action</td>
						</tr>
					</thead>
					<tbody>
						{bookingData.map(booking => {
							return (
								<tr key={booking.id}>
									<td>{booking.tour.name}</td>
									<td>
										&#x20b1; {booking.tour.price.toFixed(2)}
									</td>
									<td>
										{`${booking.user.firstName} ${booking.user.lastName}`}
									</td>
									<td>{booking.user.email}</td>
									<td>{booking.user.contact_no}</td>
									<td>
										{booking.schedule
											? format(
													parseISO(booking.schedule),
													"MM-dd-yy"
											  )
											: ""}
									</td>
									<td>
										<Button
											name="updateBtn"
											color="success"
											className="actionButton update-btn"
										>
											<i className="far fa-edit fa-lg updateIconBtn"></i>
											<span
												className="tooltip"
												id="update-tip"
											>
												update
											</span>
										</Button>

										<Button
											name="deleteBtn"
											color="danger"
											className="actionButton delete-btn"
										>
											<i className="fas fa-trash-alt fa-lg deleteIconBtn"></i>
											<span
												className="tooltip"
												id="delete-tip"
											>
												delete
											</span>
										</Button>
									</td>
								</tr>
							);
						})}
					</tbody>
				</Table>
			</Box>
		</Container>
	);
};

export default graphql(getBookingsQuery, { name: "getBookingsQuery" })(
	Bookings
);
