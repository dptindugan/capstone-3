import React from "react";
import { Container, Heading, Box, Table, Button } from "react-bulma-components";
import { graphql } from "react-apollo";
import { flowRight as compose } from "lodash";
import { getToursQuery } from "../../queries/queries";
import { deleteTourMutation } from "../../queries/mutation";
import Swal from "sweetalert2";
import { Link } from "react-router-dom";

const Tours = props => {
	// console.log(props);
	const deleteHandler = e => {
		let id = e.target.id;
		const swalWithBootstrapButtons = Swal.mixin({
			customClass: {
				confirmButton: "btn btn-success",
				cancelButton: "btn btn-danger"
			},
			buttonsStyling: true
		});

		swalWithBootstrapButtons
			.fire({
				title: "Are you sure?",
				text: "You won't be able to revert this!",
				icon: "warning",
				showCancelButton: true,
				confirmButtonText: "Yes, delete it!",
				cancelButtonText: "No, cancel!",
				reverseButtons: true
			})
			.then(result => {
				if (result.value) {
					let deleteLod = {
						id: id
					};

					props.deleteTourMutation({
						variables: deleteLod,
						refetchQueries: [
							{
								query: getToursQuery
							}
						]
					});

					swalWithBootstrapButtons.fire(
						"Deleted!",
						"successfully deleted.",
						"success"
					);
				} else if (
					/* Read more about handling dismissals below */
					result.dismiss === Swal.DismissReason.cancel
				) {
					swalWithBootstrapButtons.fire(
						"Cancelled",
						"Your file is safe :)",
						"error"
					);
				}
			});
	};

	let toursData = props.getToursQuery.getTours
		? props.getToursQuery.getTours
		: [];

	return (
		<Container>
			<Box>
				<Heading size={4} className="has-text-grey-light">
					Tours{" "}
					<a href="/tours/create" className="btn">
						<i className="far fa-plus-square fa-lg"></i>
					</a>
				</Heading>

				<Table>
					<thead>
						<tr>
							<td>Name</td>
							<td>Details</td>
							<td>Inclusions</td>
							<td>Tour Length</td>
							<td>Action</td>
						</tr>
					</thead>
					<tbody>
						{toursData.map(tour => {
							return (
								<tr key={tour.id}>
									<td>{tour.name}</td>
									<td>{tour.details}</td>
									<td>
										<ul>
											{tour.inclusions.map(
												(inclusion, index) => {
													return (
														<li key={index}>
															{inclusion}
														</li>
													);
												}
											)}
										</ul>
									</td>
									<td>{tour.tour_length}</td>
									<td>
										<Link
											to={"/tour/update/" + tour.id}
											name="updateBtn"
											className="button is-primary actionButton update-btn"
										>
											<i className="far fa-edit fa-lg updateIconBtn"></i>
											<span
												className="tooltip"
												id="update-tip"
											>
												update
											</span>
										</Link>

										<Button
											id={tour.id}
											name="deleteBtn"
											color="danger"
											className="actionButton delete-btn"
											onClick={deleteHandler}
										>
											<i
												id={tour.id}
												className="fas fa-trash-alt fa-lg deleteIconBtn"
											></i>
											<span
												className="tooltip"
												id="delete-tip"
											>
												delete
											</span>
										</Button>
									</td>
								</tr>
							);
						})}
					</tbody>
				</Table>
			</Box>
		</Container>
	);
};

export default compose(
	graphql(getToursQuery, { name: "getToursQuery" }),
	graphql(deleteTourMutation, { name: "deleteTourMutation" })
)(Tours);
