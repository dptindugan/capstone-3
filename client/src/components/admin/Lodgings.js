import React from "react";
import { Container, Heading, Box, Table, Button } from "react-bulma-components";

import { flowRight as compose } from "lodash";
import { Link } from "react-router-dom";
import { graphql } from "react-apollo";
import { getLodgingsQuery } from "../../queries/queries";
import { deleteLodgingMutation } from "../../queries/mutation";
import Swal from "sweetalert2";

const Lodgings = props => {
	const deleteHandler = e => {
		let id = e.target.id;
		const swalWithBootstrapButtons = Swal.mixin({
			customClass: {
				confirmButton: "btn btn-success",
				cancelButton: "btn btn-danger"
			},
			buttonsStyling: true
		});

		swalWithBootstrapButtons
			.fire({
				title: "Are you sure?",
				text: "You won't be able to revert this!",
				icon: "warning",
				showCancelButton: true,
				confirmButtonText: "Yes, delete it!",
				cancelButtonText: "No, cancel!",
				reverseButtons: true
			})
			.then(result => {
				if (result.value) {
					let deleteLod = {
						id: id
					};

					props.deleteLodgingMutation({
						variables: deleteLod,
						refetchQueries: [
							{
								query: getLodgingsQuery
							}
						]
					});

					swalWithBootstrapButtons.fire(
						"Deleted!",
						"successfully deleted.",
						"success"
					);
				} else if (
					/* Read more about handling dismissals below */
					result.dismiss === Swal.DismissReason.cancel
				) {
					swalWithBootstrapButtons.fire(
						"Cancelled",
						"Your file is safe :)",
						"error"
					);
				}
			});
	};

	let lodgingsData = props.getLodgingsQuery.getLodgings
		? props.getLodgingsQuery.getLodgings
		: [];

	return (
		<Container>
			<Box>
				<Heading size={4} className="has-text-grey-light">
					Lodgings
					<Link to="/lodgings/create" className="btn">
						<i className="far fa-plus-square fa-lg"></i>
					</Link>
				</Heading>

				<Table>
					<thead>
						<tr>
							<td>Name</td>
							<td>Location</td>
							<td>City</td>
							<td>Action</td>
						</tr>
					</thead>
					<tbody>
						{lodgingsData.map(lodging => {
							return (
								<tr className="dashboardDatas" key={lodging.id}>
									<td>{lodging.name}</td>
									<td>{lodging.location_id}</td>
									<td>{lodging.city}</td>
									<td>
										<Link
											to={"/lodging/update/" + lodging.id}
											name="updateBtn"
											color="success"
											className="button is-primary actionButton update-btn"
										>
											<i className="far fa-edit fa-lg updateIconBtn"></i>
											<span
												className="tooltip"
												id="update-tip"
											>
												update
											</span>
										</Link>
										<Button
											id={lodging.id}
											name="deleteBtn"
											color="danger"
											className="actionButton delete-btn"
											onClick={deleteHandler}
										>
											<i
												className="fas fa-trash-alt fa-lg deleteIconBtn"
												id={lodging.id}
											></i>
											<span
												className="tooltip"
												id="delete-tip"
											>
												delete
											</span>
										</Button>
									</td>
								</tr>
							);
						})}
					</tbody>
				</Table>
			</Box>
		</Container>
	);
};

export default compose(
	graphql(getLodgingsQuery, { name: "getLodgingsQuery" }),
	graphql(deleteLodgingMutation, { name: "deleteLodgingMutation" })
)(Lodgings);
