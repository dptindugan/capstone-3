import React, { useState, createRef } from "react";
import {
	Container,
	Button,
	Columns,
	Heading,
	Card
} from "react-bulma-components";
import { graphql } from "react-apollo";
import { updateTourMutation } from "../../queries/mutation";
import { getTourQuery } from "../../queries/queries";
import Swal from "sweetalert2";
import { flowRight } from "lodash";
import { Link } from "react-router-dom";
import { toBase64 } from "../../function.js";

const UpdateTour = props => {
	const fileRef = createRef();

	// const [imageState, setImageState] = useState({
	// 	imageInputs: [""]
	// });

	const [state, setState] = useState({
		id: "",
		details: "",
		name: "",
		itinerary: [],
		lodging_ids: "",
		availability: "",
		sub_category_id: "",
		tour_length: "",
		price: "",
		imagePath: "",
		fileName: "No File Chosen"
	});

	const [inclusionsState, setInclusionState] = useState({
		inclusions: []
	});

	// console.log(props;
	// console.log(props);

	let defaultDatas = {};
	if (!props.getTourQuery.loading) {
		let tourData = props.getTourQuery.getTour;

		defaultDatas = {
			id: tourData.id,
			details: tourData.details,
			name: tourData.name,
			itinerary: tourData.itinerary,
			lodging_ids: tourData.lodging_ids,
			availability: tourData.availability,
			sub_category_id: tourData.sub_category_id,
			tour_length: tourData.tour_length,
			price: tourData.price,
			imagePath: tourData.image
		};

		const setDefaultData = () => {
			setState(defaultDatas);
			setInclusionState({
				inclusions: tourData.inclusions
			});
		};

		if (state.id === "") {
			setDefaultData();
		}
	}

	const changeHandler = (e, index) => {
		let value = e.target.value;
		setState({
			...state,
			[e.target.name]: value
		});
	};

	const inclusionChangeHandler = (e, index) => {
		inclusionsState.inclusions[index] = e.target.value;

		setInclusionState({
			...inclusionsState,
			inclusions: inclusionsState.inclusions
		});
	};

	const addInclusions = e => {
		setInclusionState({
			inclusions: [...inclusionsState.inclusions, ""]
		});
	};

	const removeInclusionsHandler = index => {
		inclusionsState.inclusions.splice(index, 1);

		setInclusionState({
			inclusions: inclusionsState.inclusions
		});
	};

	const submitFormHandler = e => {
		e.preventDefault();

		// console.log(newTour.availability);

		if (
			defaultDatas.details === state.details &&
			defaultDatas.name === state.name &&
			defaultDatas.availability === state.availability &&
			defaultDatas.tour_length === state.tour_length &&
			defaultDatas.price === state.price &&
			defaultDatas.image === state.imagePath
		) {
			Swal.fire({
				title: "Update Failed",
				text: "No changes made",
				type: "warning",
				icon: "warning",

				// first approach
				// html:
				// 	'<a href="/" class="button is-success">Go back to members </a>',
				showCancelButton: false,
				showConfirmButton: true

				// or
				// confirmButtonText:
				// 	'<a href="/" class="has-text-white">Go back to members </a>'
			});
		} else {
			let updateTour = {
				id: state.id,
				details: state.details,
				name: state.name,
				inclusions: inclusionsState.inclusions,
				itinerary: state.itinerary,
				lodging_ids: state.lodging_ids,
				availability: true,
				sub_category_id: state.sub_category_id,
				tour_length: state.tour_length,
				price: parseFloat(state.price),
				image: state.imagePath
			};

			if (
				!updateTour.name ||
				!updateTour.details ||
				!updateTour.tour_length
			) {
				console.log(updateTour);
				Swal.fire({
					icon: "error",
					title: "Oops...",
					text: "Something went wrong!"
				});
			} else {
				// console.log(newMember);
				props.updateTourMutation({
					variables: updateTour
				});

				Swal.fire({
					title: "Tour Updated",
					text: "Tour has been updated",
					type: "success",
					icon: "success",

					// first approach
					html:
						'<a href="/" class="button is-success">Go back Dashboard</a>',
					showCancelButton: false,
					showConfirmButton: false

					// // or
					// confirmButtonText:
					// 	'<a href="/" class="has-text-white">Go back to members </a>'
				});
			}
		}
	};

	const imagePathHandler = e => {
		// console.log(fileRef.current.files[0]);
		toBase64(fileRef.current.files[0]).then(encodedFile => {
			// console.log(encodedFile);
			// console.log(fileRef.current.files[0].name);
			// console.log(index);
			// let state.fileName[0] = fileRef.current.files[fileRef.current.accessKey].name

			setState({
				...state,
				imagePath: encodedFile,
				fileName: fileRef.current.files[0].name
			});
		});
		// console.log(fileRef);
	};

	return (
		<Container>
			<Columns>
				<Columns.Column>
					<Card>
						<Card.Header>
							<Heading>Update Tour</Heading>
						</Card.Header>
						<form onSubmit={submitFormHandler}>
							<Card.Content>
								<div className="field">
									<label className="label" htmlFor="name">
										Name
									</label>
									<input
										className="input"
										id="name"
										name="name"
										type="text"
										onChange={changeHandler}
										value={state.name}
									/>
								</div>
								<div className="field">
									<label className="label" htmlFor="details">
										Details
									</label>
									<input
										className="input"
										id="details"
										name="details"
										type="text"
										onChange={changeHandler}
										value={state.details}
									/>
								</div>
								<div className="field">
									<label className="label" htmlFor="price">
										Price
									</label>
									<input
										className="input"
										id="price"
										name="price"
										type="text"
										onChange={changeHandler}
										value={state.price}
									/>
								</div>
								<div className="field">
									<Columns>
										<Columns.Column>
											<label className="label" htmlFor="inclusions">
												Inclusions
											</label>
											{inclusionsState.inclusions.map(
												(inclusion, index) => {
													return (
														<div
															key={index}
															className="inclusion"
														>
															<input
																className="input"
																name="inclusions"
																type="text"
																data-idx={index}
																onChange={e =>
																	inclusionChangeHandler(
																		e,
																		index
																	)
																}
																value={inclusion}
															/>
															<Button
																className="fas fa-times rm-inclusion-btn"
																type="button"
																onClick={() =>
																	removeInclusionsHandler(
																		index
																	)
																}
															/>
														</div>
													);
												}
											)}
											<Button
												color="success"
												type="button"
												onClick={addInclusions}
											>
												Add Inclusions
											</Button>
										</Columns.Column>
										<Columns.Column>
											<div className="field">
												<label className="label">
													Add Image
													{/*<button
															type="button"
															className="far fa-plus-square fa-lg btn"
															onClick={addImageInputHandler}
															/>*/}
												</label>

												<div className="file has-name is-fullwidth">
													<label className="file-label">
														<input
															className="file-input"
															type="file"
															accept="image/png"
															ref={fileRef}
															onChange={imagePathHandler}
														/>
														<span className="file-cta">
															<span className="file-icon">
																<i className="fas fa-upload"></i>
															</span>
															<span className="file-label">
																Choose a file…
															</span>
														</span>
														<span className="file-name">
															{state.fileName}
														</span>
													</label>
												</div>
											</div>
										</Columns.Column>
									</Columns>
								</div>
								<div className="field">
									<label className="label" htmlFor="sub_category_id">
										Location
									</label>
									<input
										className="input"
										id="sub_category_id"
										name="sub_category_id"
										type="text"
										onChange={changeHandler}
										value={state.sub_category_id}
									/>
								</div>
								<div className="field">
									<label className="label" htmlFor="tour_length">
										Tour Length
									</label>
									<input
										className="input"
										id="tour_length"
										name="tour_length"
										type="text"
										onChange={changeHandler}
										value={state.tour_length}
									/>
								</div>
							</Card.Content>
							<Card.Footer>
								<Card.Footer.Item>
									<Button fullwidth color="info">
										Update Tour
									</Button>
								</Card.Footer.Item>
								<Card.Footer.Item>
									<Link
										to="/"
										className="button is-warning is-fullwidth"
									>
										Cancel
									</Link>
								</Card.Footer.Item>
							</Card.Footer>
						</form>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default flowRight(
	graphql(updateTourMutation, { name: "updateTourMutation" }),
	graphql(getTourQuery, {
		options: props => {
			// retrieve the wildcard id param
			let id = props.match.params.id;
			return {
				variables: { id: id }
			};
		},
		name: "getTourQuery"
	})
)(UpdateTour);
