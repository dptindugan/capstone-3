import React, { useState } from "react";
import { createLodgingMutation } from "../../queries/mutation";
import {
	Container,
	Columns,
	Button,
	Heading,
	Card
} from "react-bulma-components";
import { graphql } from "react-apollo";
import Swal from "sweetalert2";

const CreateLodging = props => {
	const [state, setState] = useState({
		name: "",
		city: "",
		location_id: ""
	});

	const changeHandler = e => {
		let value = e.target.value;
		setState({
			...state,
			[e.target.name]: value
		});
	};

	const submitFormHandler = e => {
		e.preventDefault();
		let newLodging = {
			name: state.name,
			city: state.city,
			location_id: state.location_id
		};

		if (!newLodging.name || !newLodging.city || !newLodging.location_id) {
			Swal.fire({
				icon: "error",
				title: "Oops...",
				text: "Something went wrong!"
			});
		} else {
			// console.log(newMember);
			props.createLodgingMutation({
				variables: newLodging
			});

			// if()
			Swal.fire({
				position: "center",
				icon: "success",
				title: "Successfully added a new Lodging",
				showConfirmButton: false,
				timer: 1500
			});
		}
	};

	return (
		<Container>
			<Columns>
				<Columns.Column>
					<Card>
						<Card.Header>
							<Heading>Add Lodging</Heading>
						</Card.Header>

						<form onSubmit={submitFormHandler}>
							<Card.Content>
								<fieldset className="field">
									<label className="label" htmlFor="name">
										Name
									</label>
									<input
										id="name"
										className="input"
										name="name"
										onChange={changeHandler}
										value={state.name}
									/>
								</fieldset>
								<fieldset className="field">
									<label className="label" htmlFor="city">
										City
									</label>
									<input
										id="city"
										className="input"
										name="city"
										onChange={changeHandler}
										value={state.city}
									/>
								</fieldset>
								<fieldset className="field">
									<label
										className="label"
										htmlFor="location_id"
									>
										Location
									</label>
									<input
										id="location_id"
										className="input"
										name="location_id"
										onChange={changeHandler}
										value={state.location_id}
									/>
								</fieldset>
							</Card.Content>
							<Card.Footer>
								<Card.Footer.Item>
									<Button color="info">Add Lodging</Button>
								</Card.Footer.Item>
								<Card.Footer.Item>
									<Button
										renderAs="a"
										href="/"
										type="button"
										color="warning"
									>
										Cancel
									</Button>
								</Card.Footer.Item>
							</Card.Footer>
						</form>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default graphql(createLodgingMutation, {
	name: "createLodgingMutation"
})(CreateLodging);
