import React from "react";
import { Menu, Columns, Box, Heading } from "react-bulma-components";
import Bookings from "./Bookings";
import Services from "./Services";
import Transportations from "./Transportations";
import Partners from "./Partners";
import Settings from "./Settings";
import Tours from "./Tours";
import Lodgings from "./Lodgings";

const Dashboard = props => {
	const [state, setState] = React.useState({
		bookings: true
	});

	// console.log(props);

	const menuTabHandler = e => {
		setState({
			[e.target.id]: true
		});

		if (e.target.id === "logout") {
			localStorage.clear();
			window.location.href = "/login";
		}
	};

	const actionButtonHandler = e => {};

	let dashboardComponent = state.bookings ? (
		<Bookings actionBtn={actionButtonHandler} />
	) : state.services ? (
		<Services actionBtn={actionButtonHandler} />
	) : state.transportations ? (
		<Transportations actionBtn={actionButtonHandler} />
	) : state.partners ? (
		<Partners actionBtn={actionButtonHandler} />
	) : state.tours ? (
		<Tours actionBtn={actionButtonHandler} />
	) : state.lodgings ? (
		<Lodgings actionBtn={actionButtonHandler} />
	) : (
		<Settings actionBtn={actionButtonHandler} />
	);

	return (
		<Columns>
			<Columns.Column size="one-fifth">
				<Box>
					<Menu>
						<Menu.List title="Menu">
							<Menu.List.Item
								id="bookings"
								active={state.bookings}
								onClick={menuTabHandler}
							>
								Bookings
							</Menu.List.Item>
							<Menu.List.Item
								id="tours"
								active={state.tours}
								onClick={menuTabHandler}
							>
								Tours
							</Menu.List.Item>
							<Menu.List.Item
								id="services"
								active={state.services}
								onClick={menuTabHandler}
							>
								Services
							</Menu.List.Item>
							<Menu.List.Item
								id="transportations"
								active={state.transportations}
								onClick={menuTabHandler}
							>
								Transportations
							</Menu.List.Item>
							<Menu.List.Item
								id="lodgings"
								active={state.lodgings}
								onClick={menuTabHandler}
							>
								Lodgings
							</Menu.List.Item>
							<Menu.List.Item
								id="partners"
								active={state.partners}
								onClick={menuTabHandler}
							>
								Partners
							</Menu.List.Item>
							<Menu.List.Item
								id="settings"
								active={state.settings}
								onClick={menuTabHandler}
							>
								Settings
							</Menu.List.Item>
							<Menu.List.Item
								id="logout"
								actice={state.logout}
								onClick={menuTabHandler}
							>
								Logout
							</Menu.List.Item>
						</Menu.List>
					</Menu>
				</Box>
			</Columns.Column>
			<Columns.Column>
				<Heading>Dashboard</Heading>
				{dashboardComponent}
			</Columns.Column>
		</Columns>
	);
};

export default Dashboard;
